<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view("register");
    }

    public function welcome( Request $request ) {
        $firstName = $request['firstname'];
        return view('welcome', 
        [
            'firstName' => $request['firstname'],
            'lastName' => $request['lastname']
        ]);
    }
}
