<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Form</title>
</head>
<body>

    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

<form action="/welcome" method="POST">
    @csrf
    <label for="firtsname">First Name:</label><br><br>
    <input type="text" id="firstname" name="firstname" required><br><br>
    <label for="lastname">Last Name:</label><br><br>
    <input type="text" id="lastname" name="lastname" required><br><br>
    <label for="gender">Gender:</label><br><br>
    <label for="male">
        <input type="radio" name="gender" id="male">
        Male
    </label><br>
    <label for="female">
        <input type="radio" name="gender" id="female">
        Female
    </label><br>
    <label for="other">
        <input type="radio" name="gender" id="other">
        Other
    </label>
    <br><br>
    <label for="country">Nationality:</label><br><br>
    <select name="country" id="country">
        <option value="Indonesia">Indonesia</option>
        <option value="English">English</option>
        <option value="Singapore">Singapore</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Japanase">Japanase</option>
    </select><br><br>
    <label for="language">Language Spoken</label><br><br>
    <label for="bahasaindonesia">
        <input type="checkbox" name="bahasaindonesia" id="bahasaindonesia">
        Bahasa Indonesia
    </label><br>
    <label for="english">
        <input type="checkbox" name="english" id="english">
        English
    </label><br>
    <label for="other2">
        <input type="checkbox" name="other2" id="other2">
        Other
    </label><br><br>

    <label for="bio">Bio:</label><br><br>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>

    <input type="submit" value="Sign Up" id="signup">

</form>
    
</body>
</html>