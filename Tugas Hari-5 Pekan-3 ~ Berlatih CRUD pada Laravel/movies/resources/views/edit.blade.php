@extends("layouts.master")

@section("title")
  Edit Cast
@endsection

@section("content")

@error('castimage')
  <div class="alert alert-danger">
    {{ $message }}
  </div>
@enderror

@error('castname')
  <div class="alert alert-danger">
    {{ $message }}
  </div>
@enderror
@error('castage')
  <div class="alert alert-danger">
    {{ $message }}
  </div>
@enderror
@error('castbio')
  <div class="alert alert-danger">
    {{ $message }}
  </div>
@enderror

<form action="/cast/{{ $editcast->id }}" method="post">
  @csrf
  @method('PUT')

  <input type="hidden" name="oldImage" value="{{ $editcast->gambar }}">

  {{-- <div class="form-group">
    <label for="castName">Cast Image</label>
    <img src="{{ asset('storage/'.$editcast->gambar) }}" alt="" class="img-fluid" style="width: 100px; height: 100px">
    <input type="file" class="form-control" name="castname" id="castName" placeholder="Cast Name">
  </div> --}}

  <div class="form-group">
    <label for="castName">Cast Name</label>
    <input type="text" class="form-control" name="castname" id="castName" placeholder="Cast Name" value="{{ old('nama', $editcast->nama) }}">
  </div>
  <div class="form-group">
    <label for="castAge">Cast Age</label>
    <input type="text" class="form-control" name="castage" id="castAge" placeholder="Cast Age" value="{{ old('umur', $editcast->umur) }}">
  </div>

  <div class="form-group">
    <label for="castBio">Cast Bio</label>
    <input type="text" class="form-control" name="castbio" id="castBio" placeholder="Cast Bio" value="{{ old('bio', $editcast->bio) }}">
  </div>
  <button class="btn btn-primary" type="submit">Update Cast</button>
</form>

@endsection