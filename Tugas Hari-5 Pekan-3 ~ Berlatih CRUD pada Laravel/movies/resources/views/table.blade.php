@extends("layouts.master")

@section("title")
  Cast
@endsection

@section("content")

{{-- @if('success')
  <div class="alert alert-success">
      {{ session('success') }}
  </div>
@endif --}}

@if(session('cast'))
  <div class="alert alert-success">
      {{ session('cast') }}
  </div>
@endif

@if(session('edit'))
  <div class="alert alert-warning">
      {{ session('edit') }}
  </div>
@endif

@if(session('delete'))
  <div class="alert alert-success text-red-400" style="color: red;">
      {{ session('delete') }}
  </div>
@endif


<a href="/cast/create" class="btn btn-primary">Create Cast</a>

<table class="table table-bordered mt-2">
  <thead>                  
    <tr>
      <td>Id</td>
      {{-- <td>Gambar</td> --}}
      <td>Nama</td>
      <td>Umur</td>
      <td>Bio</td>
    </tr>
  </thead>
  <tbody>
    @forelse($casts as $key => $cast)
    <tr>
    <td>{{ $key + 1 }}</td>
      {{-- <td>
        @if( $cast->gambar )
        <img src="{{ asset('storage/'.$cast->gambar) }}" alt="" class="img-fluid" style="width: 100px; height:100px;">
        {{-- <i>{{ $cast->gambar }}</i> --}}
        {{-- @else --}}
          {{-- <p>Not found the image</p> --}}
        {{-- @endif --}}
        {{-- <i>{{ asset('images/'.$cast->gambar) }}</i> --}}
      {{-- </td> --}} 
      <td>{{ $cast->nama }}</td>
      <td>{{ $cast->umur }}</td>
      <td>{{ $cast->bio }}</td>
      <td class="">
        <div class="container d-flex justify-content-center align-items-center align-content-center">
        <a class="btn btn-primary" href="/cast/{{ $cast->id }}">Details Cast</a>
        <a class="btn btn-warning ml-2" href="/cast/{{ $cast->id }}/edit">Edit Cast</a>
        <form action="/cast/{{ $cast->id }}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger ml-2" type="submit">Delete Cast</button>
        </form>
      </div>

      </td>
      @empty
      <td colspan="5" class="text-center">
        Not found the data
      </td>
    </tr>
    @endforelse
  </tbody>
</table>

@endsection