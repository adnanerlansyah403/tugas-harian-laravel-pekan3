@extends("layouts.master")

@section("title")
  Create Cast
@endsection

@section("content")

@error('castname')
  <div class="alert alert-danger">
    {{ $message }}
  </div>
@enderror
@error('castage')
  <div class="alert alert-danger">
    {{ $message }}
  </div>
@enderror
@error('castbio')
  <div class="alert alert-danger">
    {{ $message }}
  </div>
@enderror

@if(session('cast'))
  <div class="alert alert-success">
    {{ $message }}
  </div>
@endif

<form action="/cast" method="post" enctype="multipart/form-data">
  @csrf

  {{-- <div class="form-group">
    <label for="castName">Cast Image</label>
    <input type="file" class="form-control" name="castimage" id="castName" placeholder="Cast Name">
  </div> --}}

  <div class="form-group">
    <label for="castName">Cast Name</label>
    <input type="text" class="form-control" name="castname" id="castName" placeholder="Cast Name">
  </div>
  <div class="form-group">
    <label for="castAge">Cast Age</label>
    <input type="text" class="form-control" name="castage" id="castAge" placeholder="Cast Age">
  </div>

  <div class="form-group">
    <label for="castBio">Cast Bio</label>
    <input type="text" class="form-control" name="castbio" id="castBio" placeholder="Cast Bio">
  </div>
  <button class="btn btn-primary" type="submit">Create Cast</button>
</form>

@endsection