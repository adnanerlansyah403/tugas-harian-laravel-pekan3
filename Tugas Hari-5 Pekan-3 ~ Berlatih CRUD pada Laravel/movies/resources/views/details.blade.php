@extends("layouts.master")

@section("title")
  Details Cast
@endsection

@section("content")

<div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
    <div class="card-header">{{ $detailscast->nama }}</div>
    <div class="card-body">
      <h5 class="card-title">{{ $detailscast->umur }}</h5>
      <p class="card-text">{{ $detailscast->bio }}</p>
    </div>
</div>

<a href="/cast" class="btn btn-primary">Back to cast</a>


@endsection