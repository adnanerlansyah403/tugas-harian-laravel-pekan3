<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class CastController extends Controller
{
    //
    public function index() {
        return view('home');
    }

    public function createcast() {
        return view('createcast');
    }

    public function store(Request $request) {

        $request->validate([
            'castname' => 'required|max:255',
            'castgambar' => 'nullable|max:255',
            'castage' => 'required|numeric|max:255',
            'castbio' => 'required'
        ],[
            'castage.numeric' => 'The castage must be a number.'
        ]);

        if($request->file('castimage')) {


            $path = $request->file('castimage')->store(
                    'public');

            $cutPath = explode('/', $path)[1];
            $queryImage = DB::table('casts')->insert([
                'gambar' => $cutPath,
                'nama' => $request['castname'],
                'umur' => $request['castage'],
                'bio' => $request['castbio']
            ]);
        } else {
            $query = DB::table('casts')->insert([
                'nama' => $request['castname'],
                'umur' => $request['castage'],
                'bio' => $request['castbio']
            ]);
        }
        
        return redirect('/cast')->with('cast', 'Berhasil menambahkan data cast');
    }

    public function detailscast($id) {
        
        $detailscast = DB::table('casts')->where('id', $id)->first();

        // dd($detailcast);

        return view('/details', compact('detailscast'));

    }

    public function editcast($id) {
        
        $editcast = DB::table('casts')->where('id', $id)->first();

        return view('/edit', compact('editcast'));

    }

    public function updatecast($id, Request $request) {

        $request->validate([
            'castimage' => 'dimensions:min_width=100,min_height=200|max:255',
            'castname' => 'required|max:255',
            'castage' => 'required|max:255',
            'castbio' => 'required|max:255'
        ],[
            'castimage.dimension' => 'Resolusi gambar harus 100x100'
        ]);

            

            if($request->file('castimage')) {

                if($request->oldImage) {
                    Storage::delete($request->oldImage);
                }

                $path = $request->file('castimage')->store(
                        'public');
    
                $cutPath = explode('/', $path)[1];
                $affected = DB::table('casts')
                ->where('id', $id)
                ->update([
                  'gambar' => $request['castgambar'],
                  'nama' => $request['castname'],
                  'umur' => $request['castage'],
                  'bio' => $request['castbio']
              ]);
            } else {
                $affected = DB::table('casts')
                ->where('id', $id)
                ->update([
                  'nama' => $request['castname'],
                  'umur' => $request['castage'],
                  'bio' => $request['castbio']
              ]);
            }

        return redirect('/cast')->with('edit', 'Berhasil memperbarui cast');
    }

    public function destroycast($id) {
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/cast')->with('delete', 'Data Cast Berhasil di hapus');
    }

    public function cast() {

        $casts = DB::table('casts')->get();

        // dd($casts);

        return view('table', ['casts' => $casts]);
    }
}
