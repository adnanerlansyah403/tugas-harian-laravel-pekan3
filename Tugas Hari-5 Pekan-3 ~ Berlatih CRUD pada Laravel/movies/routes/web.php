<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CastController@index');
Route::get('/cast', 'CastController@cast');
Route::get('/cast/create', 'CastController@createcast');
Route::post('/cast', 'CastController@store');
Route::get('/cast/{cast_id}', 'CastController@detailscast');
Route::get('/cast/{cast_id}/edit', 'CastController@editcast');
Route::put('/cast/{cast_id}', 'CastController@updatecast');
Route::delete('/cast/{cast_id}', 'CastController@destroycast');



